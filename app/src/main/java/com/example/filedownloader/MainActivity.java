package com.example.filedownloader;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText etFileUrl;

    private Button bPobierz;

    private DownloadManager downloadManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etFileUrl = findViewById(R.id.etDownloadUrl);

        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

        bPobierz = findViewById(R.id.bPobierz);
        bPobierz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //zawartosc edit texta z linkiem
                String fileUrl = etFileUrl.getText().toString();
                if (fileUrl.isEmpty()) {
                    etFileUrl.setError("Podaj link");
                    return;
                }

                //pobranie nazyw pliku z linka jezeli jest mozliwy
                String fileDetails = URLUtil.guessFileName(fileUrl, null, null);

                //zmiana linku string na Uri na potrzeby requesta
                Uri source = Uri.parse(fileUrl);

                //request pobierania
                DownloadManager.Request request = new DownloadManager.Request(source);

                //tytul pliku jezeli jest mozliwy w tytule powiadomienia
                request.setTitle(fileDetails);

                //miejsce gdzie zapiszemy plik + nazwa pliku
                request.setDestinationInExternalPublicDir(
                        Environment.DIRECTORY_DOWNLOADS + "/FileDownloader", fileDetails);

                //widocznosc notification
                request.setNotificationVisibility(
                        DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED
                );
                //w momencie kiedy pobierzemy np plik mp3 to ta linijka sprawia ze bedzie on widoczny odrazu w odtwarzaczu
                request.allowScanningByMediaScanner();

                //bez tego nie pojdzie
                long id = downloadManager.enqueue(request);

            }
        });
    }
}
